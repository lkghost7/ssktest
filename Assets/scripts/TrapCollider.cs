﻿using UnityEngine;

public class TrapCollider : MonoBehaviour{
    private GridCell cellTrigger;

    private void OnTriggerStay2D(Collider2D other) {
        if(other.gameObject.GetComponent<GridCell>() != null) {
            cellTrigger = other.gameObject.GetComponent<GridCell>();
        }
    }

    public GridCell GetCellTriggered() {
        if(cellTrigger != null) {
            return cellTrigger;
        }

        return null;
    }
}
