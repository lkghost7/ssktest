﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour{
    private List<GridCell> path;
    int                    currientIndex = 0;
    public  float          speed         = 1;
    public  bool           enemyEndMove  = false;
    private int            _enemyCountPoints;

    [SerializeField] private GridCell  currientposition;
    [SerializeField] private GridCell  endPosition;
    [SerializeField] private Transform Player;

    private PathFinding _pathFinding;
    List<GridCell>      waypoints;
    private TrapCollider _trapCollider;
    private EnemyCollider _enemyCollider;
    private GridCell _gridCell;
    private float futureTime;

    void Start() {
        _trapCollider = FindObjectOfType<TrapCollider>();
        _enemyCollider = FindObjectOfType<EnemyCollider>();
        _pathFinding       = FindObjectOfType<PathFinding>();
        transform.position = currientposition.transform.position;
        waypoints          = _pathFinding.FindPath(currientposition, endPosition);
    }

    void Update() {
        
        
        Move();
        if(_trapCollider.GetCellTriggered() != null) {
        endPosition = _trapCollider.GetCellTriggered();
        }
     

        if(enemyEndMove) {
            RefreshGrid();
        }
        
        if(Input.GetKeyDown(KeyCode.Space)) {
            _pathFinding.LoadGrid();
            enemyEndMove  = true;
            currientIndex = 0;
            waypoints     = _pathFinding.FindPath(currientposition, endPosition);
            enemyEndMove  = false;
        }
    }

   private void Move() {
        if(enemyEndMove) {
            return;
        }

        foreach(GridCell elenemt in waypoints) {
            if(elenemt.isEnd) {
                
            print(elenemt.name);
            RefreshGrid();
            return;
            }
        }

        float movmentDelta = speed * Time.deltaTime;
        try {
            
            Vector2 targetPosition = waypoints[currientIndex].transform.position;
            
            transform.position = Vector2.MoveTowards(transform.position, targetPosition, movmentDelta);
            if(Mathf.Approximately(transform.position.x, targetPosition.x)
            && Mathf.Approximately(transform.position.y, targetPosition.y)) {
                currientposition = waypoints[currientIndex];
                RefreshGrid();
                _enemyCountPoints++;

                if(currientposition == endPosition) {
                    enemyEndMove      = true;
                    currientIndex     = 0;
                    _enemyCountPoints = 0;
                }
                else {
                    if(waypoints.Count == _enemyCountPoints) {
                        enemyEndMove = true;
                        RefreshGrid();
                    }

                    currientIndex++;
                }
            }

        }
        catch(ArgumentOutOfRangeException e) {
          print("FixError");
           RefreshGrid();
        }
 
    }

    private void RefreshGrid() {
        // if(Time.time < futureTime) {
        //     return;
        // }
        //
        // futureTime = Time.time +1;
        _pathFinding.LoadGrid();
        enemyEndMove  = true;
        currientIndex = 0;
        waypoints     = _pathFinding.FindPath(currientposition, endPosition);
        enemyEndMove  = false;
    }
   
}
