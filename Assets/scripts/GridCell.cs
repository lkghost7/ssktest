﻿using UnityEngine;

// меняем какой обьект будет выбиратся в редакторе (что бы не раскрывался)
[SelectionBase]
public class GridCell : MonoBehaviour{
    public bool     isExplored = false;
    public bool     isEmpty    = true;
    public GridCell exploredFrom;
    public bool     isEnd;

    // private void Start() {
    //     isEmpty = true;
    // }

    // Метод который возвращает индекс ячейки (скрипт висит на каждой ячейке в префабе)
    public Vector2Int GetCellIndex() {
        Vector2Int index = new Vector2Int((int) transform.position.x,
                                          (int) transform.position.y);
        return index;
    }

    // метод для сброса ячейки в начальное состояние
    public void ResetCell() {
        isExplored   = false;
        exploredFrom = null;
        // SetColor(Color.white);
    }
}
