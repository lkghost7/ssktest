﻿using UnityEditor;
using UnityEngine;

// Редктор для юнити 
[CustomEditor(typeof(Grid))]
public class GridEditor : Editor{
    
    public override void OnInspectorGUI() {
        DrawDefaultInspector();
        // base.OnInspectorGUI();
        Grid grid = (Grid) target;

        // Сама кнопка
        if(GUILayout.Button("Generate")) {
            grid.ClearGrid();
            grid.GenerateGrid();
        }
    }
}
