﻿using System.Collections.Generic;
using UnityEngine;

public class PathFinding : MonoBehaviour{
    // [SerializeField] GridCell startCell;
    // [SerializeField] GridCell endCell;
    [SerializeField] private GameObject ViewPath;


    // Словарь (хэшмэп) для хранения координат ячейки, значение сама ячейка , ключ вектор2инт - коордианты этой ячейки. по мни и будем искать ячейку
    Dictionary<Vector2Int, GridCell> gridDictonary = new Dictionary<Vector2Int, GridCell>();

    // очередь для найденых элементов найденых алгаритмом 
    Queue<GridCell> queue = new Queue<GridCell>();

    // список направлений для иследования соседей в виде массива.
    private Vector2Int[] directions = {Vector2Int.up, Vector2Int.right, Vector2Int.down, Vector2Int.left,};

    void Start() {
        LoadGrid();
    }

    // метод для поиска пути
    public List<GridCell> FindPath(GridCell startCells, GridCell endCells) {
        RefreshGrid();
        queue.Enqueue(startCells);

        // искать пока есть элементы
        while(queue.Count > 0) {
            GridCell element = queue.Dequeue();

            // выйти если нашли конечную точку выйти из цикла
            if(element == endCells) {
                // путь найден и выход из цикла
                break;
            }

            //если клетка помечена как пустая только тогда вызвать иследование 
            if(element.isEmpty) {
                ExploredNeigbrours(element);
            }

            // когда закончили исследование, сделать ячейку тру.
            element.isExplored = true;
        }

        // вычеслить путь и записать его в лист
        List<GridCell> path = new List<GridCell>();

        GridCell nextCells = endCells;

        // пока exploredFrom не нал, добавлять его в лист path ( exploredFrom  откуда иследована ячейка)
        path.Add(endCells); // руками 
        while(nextCells.exploredFrom != null) {
            path.Add(nextCells.exploredFrom);
            Instantiate(ViewPath, nextCells.transform.position, Quaternion.identity);
            // брать следующий следуемый эллемент у текущей клетки
            nextCells = nextCells.exploredFrom;
        }

        path.Reverse();
        return path;

        // ExploredNeigbrours(startCells);
    }

    public void RefreshGrid() {
        // чистим очередь
        queue.Clear();
        // чистим словарь для поиска нового пути - только значения и сбрасываем флаги
        foreach(GridCell element in gridDictonary.Values) {
            element.ResetCell();
        }
    }

    // Метоод (алгаритм) для поиска соседей (соседних клеток)
    private void ExploredNeigbrours(GridCell cell) {
        // element это каждое направление по очереди вверх низ итд.
        foreach(Vector2Int element in directions) {
            Vector2Int neigbroursCoordinates = cell.GetCellIndex() + element;

            if(gridDictonary.ContainsKey(neigbroursCoordinates)) {
                GridCell neighbour = gridDictonary[neigbroursCoordinates];
                // print(neighbour.name);

                //если элемент есть в очереди или мы его исследовали.
                if(queue.Contains(neighbour) || neighbour.isExplored) {
                    // если в очереди есть уже такой элемент то ничего не делаем,
                    // что бы не проверять элементы которые уже были добавлены.
                }
                else {
                    // иначе если его нету, то добовляем его
                    //при условии что он пустой

                    queue.Enqueue(neighbour);
                    neighbour.exploredFrom = cell;
                }
            }
        }
    }

    // метод для загрузки ячеек в словарь, ищем через класс ячеки и пишем в словарь ключ (вектор2инт сами координаты), значение сама ячейка
    public void LoadGrid() {
        gridDictonary.Clear();

        GridCell[] cells = FindObjectsOfType<GridCell>();
        foreach(GridCell elementGridCell in cells) {
            gridDictonary.Add(elementGridCell.GetCellIndex(), elementGridCell);
        }
    }

    // Метод для изменения цвета
    public void SetColor(Color color) {
        MeshRenderer meshRenderer = GetComponentInChildren<MeshRenderer>();
        meshRenderer.material.color = color;
    }
}
