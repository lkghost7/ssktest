﻿using UnityEngine;

[DisallowMultipleComponent]
public class Grid : MonoBehaviour{
    [SerializeField] private GameObject cellPrefab;
    [SerializeField] private int        rows    = 10;
    [SerializeField] private int        columns = 10;

    public void GenerateGrid() {
        // цикл для создания ячеек
        for(int column = 0; column < columns; column++) {
            for(int row = 0; row < rows; row++) {
                // Позиция для каждой новой ячейки на основе цикла от нулевой точки первой ячейки.
                Vector2 position = new Vector2(row, column);
                // Создаем обьект из префаба (ячейку) на основе цикла.
                GameObject newCell = Instantiate(cellPrefab, position, Quaternion.identity);
                // назначвем каждой созданой ячеки парента (родителя)
                newCell.transform.parent = transform;
                // назначаем каждой созданной ячейке имя (в нашем случае координаты из цикла)
                newCell.name = row + "," + column;
            }
        }
    }

    // Очистить сетку, удалив все чайлды
    public void ClearGrid() {
        for(int childIndex = transform.childCount - 1; childIndex >= 0; childIndex--) {
            DestroyImmediate(transform.GetChild(childIndex).gameObject);
        }
    }
}
